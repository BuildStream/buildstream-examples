Buildstream examples: a Flatpak bundle of the NetSurf web browser
=================================================================

This project demonstrates building a simple Flatpak application bundle using
the BuildStream build tool. The application that we bundle is
[NetSurf](http://www.netsurf-browser.org/), which is a web browser designed for
resource-constrained computers.

Instructions
------------

To build the project, run the following command:

    bst build netsurf-flatpak.bst

To deploy the application to your local Flatpak repository, run the provided
deployment script:

    ./deploy.sh

Once this is done, you can run NetSurf as a Flatpak from the commandline with
this command:

    flatpak run org.netsurf_browser.NetSurf
