#!/bin/sh

set -e

OUTPUT_DIR="./netsurf-flatpak"
APP="org.netsurf_browser.NetSurf"
REF="app/${APP}/x86_64/master"
REMOTE="local-netsurf"
FLATPAK="flatpak --user"

if [ -e "${OUTPUT_DIR}" ]; then
    echo >&2 "ERROR: Output directory ${OUTPUT_DIR} already exists."
    exit 1
fi

mkdir -p "${OUTPUT_DIR}/checkout"

echo "Checking out netsurf-flatpak.bst"
bst checkout netsurf-flatpak.bst "${OUTPUT_DIR}/checkout"

echo "Committing to OSTree repository"
ostree init --repo=${OUTPUT_DIR}/repo --mode=bare
ostree commit --repo=${OUTPUT_DIR}/repo --branch $REF ${OUTPUT_DIR}/checkout
ostree summary --repo=${OUTPUT_DIR}/repo --update

echo "Installing to Flatpak"
url="file://$(pwd)/${OUTPUT_DIR}/repo"
if ${FLATPAK} remotes --show-disabled | grep -q ${REMOTE}; then
    ${FLATPAK} remote-modify ${REMOTE} --no-gpg-verify --enable --url ${url}
else
    ${FLATPAK} remote-add --no-gpg-verify ${REMOTE} ${url}
fi
${FLATPAK} install --reinstall ${REMOTE} ${APP}
