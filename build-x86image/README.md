# Building an x86image

This project builds an x86 linux image, going from our image-building
base to a fully setup busybox/musl linux image.

Refer to the [documentation]() for a step-for-step explanation.
